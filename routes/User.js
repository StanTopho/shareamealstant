const express = require("express");
const router = express.Router();
const url = require("url");

let result = {
  statusCode: 200,
  message: "Succes",
};

router.post("/", (req, res) => {
  result.message = "Succesfully registered user";
  res.send(result);
});
router.get("/", (req, res) => {
  result.message = "Succesfully retrieved all users";
  const users = [
    {
      id: 0,
      firstName: "Jan",
      lastName: "Doe",
      street: "Lovensdijkstraat 62",
      city: "Breda",
      isActive: true,
      emailAdress: "j.doe@server.com",
      password: "secret",
      phoneNumber: "06 12425475",
    },
    {
      id: 1,
      firstName: "John",
      lastName: "Doe",
      street: "Lovensdijkstraat 61",
      city: "Breda",
      isActive: true,
      emailAdress: "j.doe@server.com",
      password: "secret",
      phoneNumber: "06 12425475",
    },
  ];
  result.users = users;
  res.send(result);
});

router.post("/profile", (req, res) => {
  result.message = "Succesfully retrieved personal profile";
  const profile = {
    id: 0,
    firstName: "John",
    lastName: "Doe",
    street: "Lovensdijkstraat 61",
    city: "Breda",
    isActive: true,
    emailAdress: "j.doe@server.com",
    password: "secret",
    phoneNumber: "06 12425475",
  };
  result.profile = profile;
  res.send(result);
});

router.get("/:id", (req, res) => {
  const id = req.url.replace("/", "");
  const user = {
    id: id,
    firstName: "John",
    lastName: "Doe",
    street: "Lovensdijkstraat 61",
    city: "Breda",
    isActive: true,
    emailAdress: "j.doe@server.com",
    password: "secret",
    phoneNumber: "06 12425475",
  };
  result.user = user;
  res.send(result);
});

router.put("/:id", (req, res) => {
  const id = req.url.replace("/", "");
  const user = {
    id: id,
    firstName: req.body.firstName,
    lastName: "Doe",
    street: "Lovensdijkstraat 61",
    city: "Breda",
    isActive: true,
    emailAdress: "j.doe@server.com",
    password: "secret",
    phoneNumber: "06 12425475",
  };
  result.user = user;
  res.send(result);
});

router.delete("/:id", (req, res) => {
  const id = req.url.replace("/", "");
  result.message = `Succesfully deleted user with id: ${id}`;
  res.send(result);
});

module.exports = router;
