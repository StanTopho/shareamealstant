const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

const port = process.env.PORT || 3000;
const result = {
  code: 200,
  message: "hello  world",
};

app.get("/", (req, res) => {
  res.send("Home");
});

const userRoute = require("./routes/User");
app.use("/user", userRoute);

app.listen(port, () => {
  console.log(`"Server running on port ${port}"`);
});
